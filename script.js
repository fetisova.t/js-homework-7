/**
 * Created on 25.06.2019.
 */

//Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//Объектная можель документа, это по сути представление содержимого страницы сайта в виде объектов, у которого у каждого есть свои свойства и методы.

//Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.

function makeList(arr) {
    let newArr = arr.map(function callback(elem, i, arr) {
        return `<li>${arr[i]}</li>`;
    });
    newArr = newArr.join('');
    let list = document.createElement('ul');
    document.querySelector('script').before(list);
    list.innerHTML = newArr;
}
makeList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);